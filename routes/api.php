<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\PostController;
//use App\Http\Controllers\SubCategoryController;
use App\Http\Controllers\smsController;
use App\Http\Controllers\SubCategoryController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VerificationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//public post
Route::get('public-posts', [PostController::class, 'publicPosts']);
Route::get('public-post/{id}', [PostController::class, 'publicPost']);
Route::post('search', [PostController::class, 'searchPost']);
//public user
Route::get('users', [UserController::class, 'index']);

//public category
Route::get('public-categories', [CategoryController::class, 'publicCategories']);
Route::get('public-categories-sub-categories', [CategoryController::class, 'publicCategoriesSubcategories']);
Route::get('public-category/{id}', [CategoryController::class, 'publicCategory']);
Route::get('public-category-sub-categories/{id}', [CategoryController::class, 'publicCategorySubCategories']);

//public comments
Route::get('public-comments', [CommentController::class, 'publicComments']);
Route::get('public-comment/{id}', [CommentController::class, 'publicComment']);

//public subCategory
Route::get('public-sub-category/{id}', [SubCategoryController::class,'publicSubCategory']);

Route::get('user/{id}', [UserController::class, 'show']);

//Route::post('user', [UserController::class, 'store']);


//verify email
Route::get('email/verify/{id}', [VerificationController::class,'verify'])->name('verification.verify');
Route::post('forget-password-link', [UserController::class,'forgetPasswordLink'])->name('ForgetPassword.link');
Route::post('forget-password-update', [UserController::class,'updateForgetPassword']);
//verify sms
Route::post('sms-verification/', [UserController::class,'smsVrification']);
Route::post('resend-sms-verification/', [UserController::class,'resendSmsVerification']);

// passport
//for admin im using another middleware
Route::post('email-login',[UserController::class,'emailLogin']);
Route::post('phone-login',[UserController::class,'phoneLogin']);
Route::post('register', [UserController::class,'register']);
Route::post('avatar', [UserController::class,'uploadAvatar']);


Route::group(['middleware' => ['auth:api','admin']], function()
{
    Route::post('category', [CategoryController::class, 'store']);
    Route::get('categories', [CategoryController::class, 'index']);
    Route::get('category/{id}', [CategoryController::class, 'show']);
    Route::put('category/{id}', [CategoryController::class, 'update']);
    Route::delete('category/{id}', [CategoryController::class, 'destroy']);
    Route::get('comments-admin', [CommentController::class, 'adminIndex']);
    Route::get('comment-admin/{id}', [CommentController::class, 'AdminShow']);
// instead of 4 routes u can se this one all together
    Route::resource('sub-category','SubCategoryController');
});


Route::group(['middleware' => 'auth:api'], function() {
    Route::get('details', [UserController::class, 'details']);
//post
    Route::get('posts', [PostController::class, 'index']);
    Route::post('post', [PostController::class, 'store']);
    Route::put('post/{id}', [PostController::class, 'update']);
    Route::get('post/{id}', [PostController::class, 'show']);
    Route::delete('post/{id}', [PostController::class, 'destroy']);
//user
    Route::put('user', [UserController::class, 'update']);
    Route::delete('user', [UserController::class, 'destroy']);
    Route::delete('user-deactivate', [UserController::class, 'userDeactivate']);
//verify
    Route::get('email/resend', [UserController::class, 'resend'])->name('verification.resend');
//tag
    Route::post('tag', [TagController::class, 'store']);
    Route::get('tags', [TagController::class, 'index']);
    Route::get('tag/{id}', [TagController::class, 'show']);
    Route::put('tag/{id}', [TagController::class, 'update']);
    Route::delete('tag/{id}', [TagController::class, 'destroy']);
    Route::post('search-tag', [TagController::class, 'searchTag']);

//    comment
    Route::post('comment', [CommentController::class, 'store']);
    Route::post('comment/{id}/reply', [CommentController::class, 'createCommentReply']);
    Route::put('comment/{id}', [CommentController::class, 'update']);
    Route::delete('comment/{id}', [CommentController::class, 'destroy']);
    Route::put('approve-comment/{id}', [CommentController::class, 'approveComment']);
    Route::get('comments', [CommentController::class, 'index']);
    Route::get('comment/{id}', [CommentController::class, 'show']);

});


//Route::get('post/{id}/random', [PostController::class, 'getThreeRandomPost']);
