<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubCategoryResource extends JsonResource
{
    public function toArray($request)
    {
        return [
          'id'=>  $this->id,
          'name'=>  $this->name,
//          'description'=>  $this->description,
//            // category in model is the relation method
//          'category'=>  CategoryResource::make($this->category),

        ];
    }
}
