<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SmsLoginRequest extends FormRequest
{

    public function rules()
    {
        return [
            'phone_number' => 'required|string|numeric|digits:11',
            'password' => 'required|string|min:6|max:16',

        ];
    }
}
