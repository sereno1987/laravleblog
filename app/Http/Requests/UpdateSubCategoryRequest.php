<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSubCategoryRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'string',
            'description' => 'string',
            'category_id' => 'int|exists:categories,id',
        ];
    }
}
