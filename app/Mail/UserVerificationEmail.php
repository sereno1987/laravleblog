<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserVerificationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;

    public $link;

    public function __construct($name, $link)
    {
        $this->name = $name;
        $this->link = $link;
    }


    public function build()
    {
        return $this->subject('VERIFICATION EMAIL')
            ->view('emails.verify-email');
    }
}
