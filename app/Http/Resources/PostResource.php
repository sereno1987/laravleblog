<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'title'=> $this->title,
            'content'=> $this->content,
            'featured_image'=> $this->featured_image,
            'category' => CategoryResource::make($this->category),
            'user' => UserPerUserResource::make($this->user),
            'tag'=> TagResource::collection($this->tags),
            'comments'=> CommentResource::collection($this->comments)
        ];
    }
}
