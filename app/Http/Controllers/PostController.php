<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\PostResource;
use App\Http\Resources\UpdatePostResource;
use App\Http\Resources\UserResource;
use App\Post;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function publicPosts(Request $request)
    {
        $isFeatured  = $request->get('featured');
        $hasImage  = $request->get('featured_image');

        $posts = Post::query()->with([ 'comments'=> function($q) {
            $q->where('approved_at', '!=', null)->where('reply_id',null) ;
        },'comments.replies'=>function($q) {
            $q->where('approved_at', '!=', null);
        }])
            ->where('published',true)
            ->whereHas('user', function ($query) {
                 $query->where('deactivated_at',null);
            });

        if ($isFeatured!==null) {
            $posts->where('featured', $isFeatured);
        }

        if ($hasImage) {
            $posts->whereNotNull('featured_image');
        }

        return response()->json(PostResource::collection($posts->get()));

    }

    public function publicPost($id)
    {
        $post = Post::query()
            ->with([ 'comments'=> function($q) {
                    $q->where('approved_at', '!=', null)->where('reply_id',null) ;
            },'comments.replies'=>function($q) {
                $q->where('approved_at', '!=', null);
            }])
            ->where('published',true)
            ->whereHas('user', function ($query) {
                $query->where('deactivated_at',null);
            })
            ->findOrFail($id);



        return response()->json(PostResource::make($post));
    }

    public function index(Request $request)
    {
        $isPublished = $request->get('published');
        $isFeatured  = $request->get('featured');
        $hasImage  = $request->get('featured_image');

        $posts = Post::query()
            ->with([ 'comments'=> function($q) {
                $q->where('approved_at', '!=', null)->where('reply_id',null) ;
            },'comments.replies'=>function($q) {
                $q->where('approved_at', '!=', null);
            }])
            ->where('user_id', Auth::user()->id);

        if ($isPublished!==null) {
            $posts->where('published', $isPublished);
        }

        if ($isFeatured!==null) {
            $posts->where('featured', $isFeatured);
        }

        if ($hasImage) {
            $posts->whereNotNull('featured_image');
        }

        return response()->json(PostResource::collection($posts->get()));

    }

    public function show($id)

    {
        $post = Post::query()
            ->with([ 'comments'=> function($q) {
        $q->where('approved_at', '!=', null)->where('reply_id',null) ;
    },'comments.replies'=>function($q) {
        $q->where('approved_at', '!=', null);
    }])
        ->where('user_id', Auth::user()->id)
        ->find($id);

        return response()->json(PostResource::make($post));
    }

    public function store(CreatePostRequest $request)
    {
        // php artisan make:request  CreatePostRequest --for validation
        //php artisan make:resource PostResource
        $post = Post::query()->create([
            'title'=> $request->get('title'),
            'slug'=> $request->get('slug'),
            'content'=> $request->get('content'),
            'category_id'=> $request->get('category_id'),
            'featured_image'=> $request->get('featured_image'),
            'user_id'=> Auth::user()->id,
            'featured'=> $request->get('featured'),
            'published'=> $request->get('published'),
        ]);

        $post->tags()->attach($request->get('tags_id'));

        return response()->json(PostResource::make($post));
    }

    public function update(UpdatePostRequest $request, $id)
    {
        $post=Post::query()
            ->where('user_id',Auth::user()->id)
            ->whereHas('user', function ($query) {
                $query->where('deactivated_at', null);
            })
            ->findOrFail($id);

        $post->update([
            'title'=> $request->get('title', $post->title),
            'slug'=> $request->get('slug', $post->slug),
            'content'=> $request->get('content',$post->content),
            'category_id'=> $request->get('category_id',$post->category_id),
            'featured_image'=> $request->get('featured_image',$post->featured_image),
            'featured'=> $request->get('featured',$post->featured),
            'published'=> $request->get('published',$post->published),
        ]);
        return response()->json(UpdatePostResource::make($post));
    }

    public function destroy($id)
    {
        $post = Post::query()->where('user_id',Auth::user()->id)->findOrFail($id);
        $post->delete();
        return response()->json([]);

    }

    public function searchPost(Request $request)
    {
       $data=trim($request->get('data'));
       $postSearchResult = Post::with(['user'])
           ->where('published',1)
           ->where(function($q) use($data) {
               $q->Where('content', 'like', "%{$data}%")
                   ->orWhere('title', 'like', "%{$data}%");
           })
           ->whereHas('user', function ($query) {
               $query->where('deactivated_at', null);
           })
           ->get();

        return response()->json(['data'=> $postSearchResult]);
    }



//    public function getThreeRandomPost($id)
//    {
//        $postCategoryId = Post::query()->find($id)->category->id;
//        $posts=Post::query()->where('published',true)->where('category_id', $postCategoryId)->take(3)->get();
//
//        return response()->json($posts);
//    }
}
