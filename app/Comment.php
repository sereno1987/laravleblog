<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    protected $fillable=[
        'body',
        'user_id',
        'post_id',
        'approved_at',
        'reply_id'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function replies()
    {
        return $this->hasMany(Comment::class,'reply_id', 'id');
    }

    public function approvedReplies()
    {
        return $this->replies()->whereNotNull('approved_at');
    }

    public function unApprovedRepliesCount()
    {
        return $this->replies()->whereNull('approved_at')->count();
    }

    public function scopeUnapproved($query)
    {
        return $query->whereNull('approved_at');

    }

    public function scopeApproved($query)
    {
        return $query->whereNotNull('approved_at');

    }

    public function comment()
    {
        return $this->belongsTo(Comment::class,'reply_id','id');
    }
}
