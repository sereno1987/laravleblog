<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CreateCategorytRequest;
use App\Http\Requests\CreateTagRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Http\Requests\UpdateTagRequest;
use App\Http\Resources\TagResource;
use App\Post;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class TagController extends Controller
{
    public function index()
    {
        $tags=Tag::query()->where('user_id',Auth::user()->id)->get();
        //instead of make use collection for more than one data
        return response()->json(TagResource::collection($tags));
    }

    public function show($id)
    {
        $tag = Tag::query()->where('user_id',Auth::user()->id)->findOrFail($id);

        return response()->json(TagResource::make($tag));
    }

    public function store(CreateTagRequest $request)
    {
        // php artisan make:request  CreateTagRequest
        $tagExists = Tag::query()->where([
            'name'=> $request->get('name'),
            'slug'=> $request->get('slug'),
            'user_id'=> Auth::user()->id,
        ])->exists();

        if ($tagExists) {
            return response()->json(['message' => 'tag already exists.']);
        }

        $tag= Tag::query()->create([
            'name'=> $request->get('name'),
            'slug'=> $request->get('slug'),
            'description'=> $request->get('description'),
            'user_id'=> Auth::user()->id,
        ]);

        return response()->json(TagResource::make($tag));
    }

    public function update(UpdateTagRequest $request, $id)

    {
        $tag=Tag::query()->where('user_id',Auth::user()->id)->findOrFail($id);

        $tagExists = Tag::query()->where([
            'name'=> $request->get('name'),
            'slug'=> $request->get('slug'),
            'user_id'=> Auth::user()->id,
        ])->exists();

        if ($tagExists) {
            return response()->json(['message' => 'tag already exists.']);
        }

        $tag->update([
            'name'=> $request->get('name', $tag->name),
            'slug'=> $request->get('slug', $tag->slug),
            'description'=> $request->get('description',$tag->description),
        ]);
        return response()->json(TagResource::make($tag));
    }
    public function destroy($id)
    {
        $tag = Tag::query()->where('user_id',Auth::user()->id)->findOrFail($id);
        $tag->delete();
        return response()->json([]);

    }

    public function searchTag(Request $request)
    {

        $data=trim($request->get('data'));
        $tagSearchResult = Tag::query()->where('user_id',Auth::user()->id)->where('name', 'like', "%{$data}%")->get();
        return response()->json(TagResource::collection($tagSearchResult));
    }



}
