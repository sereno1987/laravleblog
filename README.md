**Auth -> postman**
 Guest user register by (register)
- Name
- Email or phone
- Password
- C-password
- Avatar: nullable

    After registration a user with an editor role will be created..
- Guest User receive an email with verification link and by clicking the email would be verified
- There is expiration time and you can request to resend the link(emailResend)
- Guest User receive a code via SMS  by entering the code, user would be verified
- There is expiration time for code and you resend the code (resendSmsVerification)
- Guest user can login with email if email is verified(emailLogin)
- Guest user can login with phone if phone is verified(smsLogin)
- Guest User can request for forget password and a link will be send to email and will enter the new password(forgetPasswordByEmail and forgetPasswordUpdate)
- Guest user Forget pass by phone
- Logged in user can see own user info (details)


**Role ->  postman (all admins can do followings)**

- Admins can create a role by giving a name to it(createRole)
- Admins can delete a role (deleteRole)
- Admins can update a role (updateRole)
- Admins can see list of roles (listRoles)
- Admins can see a role (getRoleById)

**Assign-permission-to-role ->  postman (all admins can do followings)**

- Admins can assign permissions to a role(AssignPermissionToRole)
- Admins can edit given permissions to a role(updateAssignToRolePermission)
- Admins can delete assigned role with permissions(deleteAssignToRolePermission)
- Admins can see a role with assigned permissions(getRolePermissionById)
- Admins can see list of roles with assigned permissions(RolePermissionList)


**Permission ->  postman (all admins can do followings)**

- Admins can create a permission by giving a name to it(createPermission)
- Admins can delete a permission (deletePermission)
- Admins can update a permission (updatePermission)
- Admins can see list of permissions (listPermissions)
- Admins can see a permission (getPermissionById)


**assign-role-to-user ->  postman (all admins can do followings)**

- Admins can assign,update and delete the roles of the user . ex: editor to admin (assignRoleToUser)
- Admins can list all the users and their roles with permissions. (UserRolesList)
- Admins can see a particular user with his roles with permissions. (getUserRolesById)


**User ->  postman**

- Logged in user   can deactivate the account so nothing would be visible from that user user cant log in anymore (deactivateUser)
- Logged in User can delete the account so nothing related to user would be remained (deleteUser)
- Logged in User can update name and avatar(updateUser, avatarURL)
- Guest user can see all active users who are verified by email or phone (listusers)
- Guest user can see the details for one active user who are verified by email or phone(getUserById)

**post ->  postman**
    
- Guest users can see all published posts from active users , category,tags,user info and comments and comments’ replies .(publicPosts)
- Guest users can see one published post from active users , category,tags,user info and comments and comments’ replies.(publicPost)
- Guest users can filter posts by “is featured” field .(publicPosts)
- Guest users can filter posts by “has image” field.(publicPosts)
- Logged in user can see all his own posts(ListPosts)
- Logged in user can see the details of his own post (getPostById)
- Logged in user can delete his own post(deletePosts)
- Logged in user can update his own post(updatePost)
- User can search the specific word in title or body of a post and will get the published posts where the writer is active(searchPost)
- 


**category ->  postman      admin users can create,delete and update categories**

- all Us- ers can see the list of all categories (publicCategories)
- Guest User can see the published posts related to the specific category from active users (publicCategory)   
- Admin can see the details of a category(getCategoryById)
- Admin can see the list of his own categories (listCategories)
- Admin user can update the category which he is the owner (UpdateCategory)
- Admin user can create the category (createCategory)
- Admin user can delete the category which he is the owner (deleteCategory)
- User can see the list of categories and subcategories(publicCategoriesSubCategories)
- User can see the specific categor
y with all subcategories(publicCategorySubCategories)

**subcategory ->  postman      admin users can create,delete and update categories**

- Admin can see the details of a subCategory(getsubCategoryById)
- Admin can see the list of all subCategories (listSubCategories)
- Admin user can update the subCategory which he is the owner (UpdateSubCategory)
- Admin user can create the category - repeated subcat couldn't exist in a specific category(createSubCategory)
- Admin user can delete the subCategory which he is the owner (deleteSubCategory)
- User by choosing a sub category can see the list of the posts marked by that category(publicSubCategory)


**tag ->  postman    users can create,delete and update their own tags**

- Logged in user can create tag (unique tag name and slug for specific user )(createTag)
- User can see the list of his own tags (listTags)
- User can see the details of a tag of his own (getTagById)
- User can update the tag of his own(updateTag)
- User can delete  the tag of his own(deleteTag)
- User can search his own tag name (searchTag)

**comment ->  postman**
- Logged in user can post a comment(creatComment)
- Logged in user can update a comment of his own-after updating it needs verification again (updateComment) 
- Logged in user can delete a comment of his own(deleteComment)
- Logged in user can see the list of comments (approved and not approved)  related to his own posts and filter them (listComments)
- Logged in user can see the details of a comment (approved and not approved) related to his own posts(getCommentById)
- Users can see the list of approved comments with approved replies with number of replies (publicComments)
- user can see the detail of an approved comment with approved replies wih number of replies (publicComment)
- Admin can see the list of all comments and filter approved or not(adminListComment)
- Admin can see the detail of a comment (AdmingetCommentById)
- Admin can approve and disapprove field of comment(approveComment)
- Logged in user can post a reply for a comment(createCommentReply)




