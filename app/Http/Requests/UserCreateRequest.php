<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required_without:phone_number|email|unique:users,email',
            'phone_number' => 'required_without:email|numeric|unique:users,phone_number',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ];
    }
}
