<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmailLoginRequest;
use App\Http\Requests\ForgetPasswordLinkRequest;
use App\Http\Requests\ForgetPasswordUpdateRequest;
use App\Http\Requests\resendSmsVerificationRequest;
use App\Http\Requests\SmsLoginRequest;
use App\Http\Requests\SmsVerificationRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserCreateRequest;
use App\Http\Resources\UserPerUserResource;
use App\Http\Resources\UserResource;
use App\Mail\ForgetPasswordemail;
use App\Mail\UserVerificationEmail;
use App\PasswordReset;
use App\User;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Kavenegar\KavenegarApi;
use LogicException;
use Throwable;
use function MongoDB\BSON\toCanonicalExtendedJSON;

class UserController extends Controller
{
    public $successStatus = 200;

    public function index()
    {
        $users=User::query()
            ->whereNull('deactivated_at')
            ->where(function($q)  {
            $q->whereNotNull('email_verified_at')
            ->orWhereNotNull('phone_verified_at');
    })->get();
        return response()->json(UserPerUserResource::collection($users));
    }

    public function show($id)
    {
        $user = User::query()
            ->whereNull('deactivated_at')
            ->where(function($q)  {
                $q->whereNotNull('email_verified_at')
                    ->orWhereNotNull('phone_verified_at');
            })->findOrFail($id);

        return response()->json(UserPerUserResource::make($user));
    }

//    public function store(UserCreateRequest $request)
//    {
////         php artisan make:request  CreateUserRequest
//        $user = User::query()->create([
//            'name'=> $request->get('name'),
//            'role_id'=> $request->get('role_id'),
//            'email'=> $request->get('email'),
//            'avatar'=> $request->get('avatar'),
//            'password'=> $request->get('password'),
//        ]);
//
//        return response()->json(UserResource::make($user));
//    }

    public function update(UpdateUserRequest $request)
    {
        $user=User::query()->find(Auth::user()->id);

        $user->update([
            'name'=> $request->get('name', $user->name),
            'password'=> $request->get('password') ? bcrypt($request->get('password')) : $user->password,
            'avatar'=> $request->get('avatar', $user->avatar)
        ]);
        return response()->json(UserPerUserResource::make($user));
    }

    public function updateForgetPassword(ForgetPasswordUpdateRequest $request)
    {
        $passwordResetUser = PasswordReset::query()->where('token', $request->get('token'))->first();
        $user = User::query()->where('email', $passwordResetUser->email)->first();
        $user->update([
            'password'=>bcrypt($request->get('password'))
        ]);
        return response()->json(UserPerUserResource::make($user));
    }

    public function destroy()
    {
        $user = User::query()->find(Auth::user()->id);
        $user->delete();
        return response()->json(null,204);

    }

    public function emailLogin(EmailLoginRequest $request){
        try {

            if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
                $user = Auth::user();
                if ($user->deactivated_at == null and $user->email_verified_at != null) {
                    $user['token'] = $user->createToken('MyApp')->accessToken;
                    return response()->json(UserResource::make($user));
                } else {
                    return response()->json(['error' => 'Deactivated or email not verified'], 403);
                }

            } else {
                return response()->json(['error' => 'Unauthorised'], 401);
            }
        }
        catch (Throwable $exception) {
            return response()->json(['Please try again!'], 403);
        }
    }

    public function phoneLogin(SmsLoginRequest $request){
        try {

            if(Auth::attempt(['phone_number' => $request->get('phone_number'), 'password' => $request->get('password')])){
                $user = Auth::user();
                if($user->deactivated_at==null and $user->phone_verified_at!=null)
                {
                    $user['token'] =  $user->createToken('MyApp')-> accessToken;
                    return response()->json(UserResource::make($user));
                }
                else{
                    return response()->json(['error'=>'Deactivated or not verified'], 403);
                }

            }
            else{
                return response()->json(['error'=>'Unauthorised'], 401);
            }
        } catch (Throwable $exception) {
            return response()->json(['Please try again!'], 403);
        }
    }

    public function register(UserCreateRequest $request)
    {
        try {
            $input = $request->all();

            $input['password'] = bcrypt($input['password']);

            DB::beginTransaction();
            $user = User::query()->create($input);
            DB::commit();
            if ($user->email){
                $this->sendVerificationEmail($user);
            }
            if ($user->phone_number){
                $this->sendVerificationSMS($user);

            }
            return response()->json(UserPerUserResource::make($user));
        } catch (Throwable $exception) {
            return response()->json([$exception->getMessage()], 403);
        }
    }

    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this-> successStatus);
    }

    public function userDeactivate()
    {
        $user = Auth::user();

        $user->update([
            'deactivated_at'=>now()
        ]);
        return response()->json(null,204);

    }

    public function uploadAvatar (Request  $request)
    {
        Storage::put('uploads/', $request->image);
        $url = Storage::url($request->image);

        return response()->json(['file' => 'http://localhost:8000'.$url]);
    }

//------------------------------------------------------------------------------------------Email verification
    private function sendVerificationEmail($user)
    {
        $verificationLink = 'localhost:8000/api/email/verify/'.$user->id.'?'.$this->getSignedLinkQuery('verification.verify', ['id', $user->id], Carbon::now()->addHours(4));

        Mail::to($user->email)->send(new UserVerificationEmail($user->name, $verificationLink));
    }

    public function forgetPasswordLink(ForgetPasswordLinkRequest $request)
    {
        $user = User::query()->where('email',$request->get('email'))->first();
        if ($user){
            if ($user->deactivated_at != null){
                return response()->json(['message'=>"user is deactivated"]);
            }
            $passwordReset=PasswordReset::query()->create(['email'=>$request->get('email'),'token'=> Str::uuid()]);
            $forgetPasswordLink = 'localhost:8000/api/email/forget-password/'.'?token='.$passwordReset->token;

            Mail::to($user->email)->send(new ForgetPasswordemail($user->name, $forgetPasswordLink));
        }
        else{
            return response()->json(['error'=>'user doesnt exist'], 404);

        }

    }
// for logged in users
    public function resend()
    {
        if (auth()->user()->hasVerifiedEmail()) {
            return response()->json(["msg" => "Email already verified."], 400);
        }

        $this->sendVerificationEmail(auth()->user());

        return response()->json(["msg" => "Email verification link sent on your email id"]);
    }

    public function sendVerificationSMS($user)
    {

        $verificationCode= rand(1000,100000);
        $user->update([
            'verification_code'=> $verificationCode
        ]);
        $this->sendSMS($verificationCode,[$user->phone_number]);

    }

    //---------------------------------------------------------------------------------------SMS Verification
    public function smsVrification(SmsVerificationRequest $request)
    {
        try {
            $verificationCode = $request->get('verification_code');
            $phoneNumber = $request->get('phone_number');
            $user = User::query()->where('phone_number', $phoneNumber)->first();
            if ($user->phone_verified_at == null) {

                if ($user->updated_at->addMinute() > now()) {
                    if ($user->verification_code == $verificationCode) {
                        $user->update([
                            'phone_verified_at' => now()
                        ]);
                        return response()->json(["msg" => "verified successfully"]);
                    } else {
                        return response()->json(["msg" => "check the code"], 422);

                    }
                } else {

                    $verificationCode = rand(1000, 100000);
                    $user->update([
                        'verification_code' => $verificationCode
                    ]);
                    return response()->json(["msg" => "code has been expired"], 422);
                }
            } else {
                return response()->json(["msg" => "this user has been already verified"], 422);
            }
        } catch (Throwable $exception) {
            return response()->json(['Please try again!'], 403);
        }
    }

    public function resendSmsVerification(resendSmsVerificationRequest $request){
        $user = User::query()->where('phone_number', $request->get('phone_number'))->first();
        if ($user->phone_verified_at == null){
            $verificationCode= rand(1000,100000);
            $user->update([
                'verification_code'=> $verificationCode
            ]);
            $this->sendSMS($verificationCode,[$user->phone_number]);
        }
        else{
            return response()->json(["msg" => "this user has been already verified"], 422);
        }

    }

    public function sendSMS($message, $receptor){
        try{
            $api = new KavenegarApi( "4641634F6543765834453256355177595777794378622F616D4838764B505A4A4443574D6930506B6872513D" );
            $sender = "1000596446";
            $result = $api->Send($sender,$receptor,$message);
            if($result){
                foreach($result as $r){
                    echo "messageid = $r->messageid";
                    echo "message = $r->message";
                    echo "status = $r->status";
                    echo "statustext = $r->statustext";
                    echo "sender = $r->sender";
                    echo "receptor = $r->receptor";
                    echo "date = $r->date";
                    echo "cost = $r->cost";
                }
            }
        }
        catch(\Kavenegar\Exceptions\ApiException $e){
            // در صورتی که خروجی وب سرویس 200 نباشد این خطا رخ می دهد
            echo $e->errorMessage();
        }
        catch(\Kavenegar\Exceptions\HttpException $e){
            // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد
            echo $e->errorMessage();
        }
    }
}
