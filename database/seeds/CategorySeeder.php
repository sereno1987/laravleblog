<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    public function run()
    {
        $categories = array(['sports','sports',1],['skin care','skin care',1],['yoga','yoga',1]);
        foreach ($categories as $category){
        Category::create([
            'name' => $category[0],
            'slug' => $category[1],
            'user_id' => $category[2],
        ]);

    }

    }
}
