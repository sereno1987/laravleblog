<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ForgetPasswordLinkRequest extends FormRequest
{

    public function rules()
    {
        return [
            "email"=>'required|email'
        ];
    }
}
