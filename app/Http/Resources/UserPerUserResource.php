<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserPerUserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'name'=> $this->name,
//            'email'=> $this->email,
//            'avatar'=> $this->avatar,
//            'is_admin'=> $this->is_admin,
//            'phone_number'=> $this->phone_number,
//            'deactivated_at'=> $this->deactivated_at,
//            'ephone_verified_at'=> $this->phone_verified_at,
//            'email_verified_at'=> $this->email_verified_at,
        ];
    }
}
