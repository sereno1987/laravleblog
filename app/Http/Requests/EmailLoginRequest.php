<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmailLoginRequest extends FormRequest
{

    public function rules()
    {
        return [
            'email' => 'required|string|email',
            'password' => 'required|string|min:6|max:16',
        ];
    }
}
