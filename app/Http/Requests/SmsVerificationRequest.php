<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SmsVerificationRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_number' => 'required|numeric',
            'verification_code' => 'required',
        ];
    }
}
