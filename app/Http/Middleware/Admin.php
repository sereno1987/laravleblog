<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            if (auth()->user()->is_admin != 1) {
                return response()->json(['message'=>'you are not admin user.'], 403);
            }

        return $next($request);
    }
}
