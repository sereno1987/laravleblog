<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'body'=> $this->body,
            'approved_at'=> $this->approved_at,
            'user'=> UserPerUserResource::make($this->user),
            'replies'=> CommentResource::collection($this->replies),
            'replyCount'=> count($this->replies),
        ];
    }
}
