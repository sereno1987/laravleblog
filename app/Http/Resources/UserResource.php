<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'name'=> $this->name,
            'email'=> $this->email,
            'pnone_number'=> $this->phone_number,
            'avatar'=> $this->avatar,
            'token'=> $this->token,
            'deactivated_at'=> $this->deactivated_at,
        ];
    }
}
