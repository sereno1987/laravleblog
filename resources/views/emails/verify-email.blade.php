    <tbody>
    <tr>
        <td colspan="2" style="padding: 52px 65px 20px;">
            <p style="margin: 0 0 5px; font-size: 14px; color: #303030;">
                Hello, <b style="font-size: 14px; color: #303030;">{{$name}}</b>!
            </p>
            <p style="margin: 0 0 5px; font-size: 12px; color: #303030;">
                Please confirm your email address by
                tapping into the button below and get ready to discover a full feature seamless experience.
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 50px 0; text-align: center;">
            <p style="margin: 0; padding: 0;">
                <a href="{{$link}}" style="margin: 0 auto; display: block; border-radius: 24px; width: 180px; height: 48px; color: #FFFFFF; background: #9C2780; font-size: 12px; line-height: 48px; text-decoration: none; font-weight: bold; text-transform: uppercase;">
                    Confirm
                </a>
            </p>
        </td>
    </tr>
    </tbody>
