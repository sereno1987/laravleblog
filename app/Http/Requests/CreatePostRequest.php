<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePostRequest extends FormRequest
{
    public function rules()
    { 
        return [
            'slug' => 'required|string|unique:posts,slug',
            'content' => 'required|string',
            'title'=> 'required|string',
            'category_id'=> 'required|int|exists:categories,id',
        ];
    }
}
