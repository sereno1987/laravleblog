<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CreateCategorytRequest;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CategorySubcategoryResource;
use App\Http\Resources\CategyPostResource;
use App\Http\Resources\PostResource;
use App\Post;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function MongoDB\BSON\toCanonicalExtendedJSON;

class CategoryController extends Controller
{
    public function publicCategories()
    {
        $categories=Category::all();
        return response()->json(CategoryResource::collection($categories));
    }

    public function publicCategoriesSubcategories()
    {
        $categories=Category::all();
        return response()->json(CategorySubcategoryResource::collection($categories));
    }

    public function  publicCategorySubCategories($id)
    {
        $categories = Category::where('id',$id)->get();

        return response()->json(CategorySubcategoryResource::collection($categories));
    }

    public function publicCategory($id)
    {
        $posts = Post::with(['user'])->where('category_id',$id)->where('published',1)
            ->whereHas('user', function ($query) {
                $query->where('deactivated_at', null);
            })->get();

        return response()->json(PostResource::collection($posts));
    }

    public function index()
        {
            $categories=Category::query()->where('user_id',Auth::user()->id)->get();
            return response()->json(CategoryResource::collection($categories));
        }

    public function show($id)
    {
            $category = Category::query()->findOrFail($id);

            return response()->json(CategoryResource::make($category));
    }

    public function store(CreateCategorytRequest $request)
    {
        // php artisan make:request  CreateCategorytRequest
        $category= Category::query()->create([
            'name'=> $request->get('name'),
            'slug'=> $request->get('slug'),
            'description'=> $request->get('description'),
            'user_id'=> Auth::user()->id,
        ]);
        return response()->json(CategoryResource::make($category));
    }

    public function update(UpdateCategoryRequest $request, $id)
    {
        $category=Category::query()->where('user_id',Auth::user()->id)->findOrFail($id);

        $category->update([
            'name'=> $request->get('name', $category->name),
            'slug'=> $request->get('slug', $category->slug),
            'description'=> $request->get('description',$category->description),
        ]);
        return response()->json(CategoryResource::make($category));
    }

    public function destroy($id)
    {
        $category = Category::query()->where('user_id',Auth::user()->id)->findOrFail($id);
        $category->delete();
        return response()->json([]);

    }
}
