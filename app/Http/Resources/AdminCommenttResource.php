<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdminCommenttResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'body'=> $this->body,
            'approved_at'=> $this->approved_at,
            'user'=> UserPerUserResource::make($this->user),
//            'replies'=> ($this->replies),
            'replyCount'=> count($this->replies),
        ];
    }
}
