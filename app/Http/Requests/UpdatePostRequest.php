<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => 'string|unique:posts,slug',
            'content' => 'string',
            'title'=> 'string',
            'category_id'=> 'int|exists:categories,id',
            'user_id'=> 'int|exists:users,id',
        ];
    }
}
