<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'email' => 'string|email|unique:users,email',
            'phone_number' => 'string|numeric|digits:11',
            'password' => 'string|min:6|max:16',
            'role_id' => 'int|exists:users,role_id',
        ];
    }
}
