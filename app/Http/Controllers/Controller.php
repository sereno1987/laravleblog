<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\URL;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function getSignedLinkQuery(string $routeName, array $parameters = [], $expiration = null, bool $absolute = true)
    {
        $url = URL::signedRoute($routeName, $parameters, $expiration, $absolute);

        return parse_url($url)['query'];
    }
}
