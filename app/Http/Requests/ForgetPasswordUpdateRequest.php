<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ForgetPasswordUpdateRequest extends FormRequest
{

    public function rules()
    {
        return [
            'token'=>'required',
            'password' => 'required',
            'c_password' => 'required|same:password'
        ];
    }
}
