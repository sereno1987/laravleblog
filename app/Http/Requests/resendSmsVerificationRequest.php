<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class resendSmsVerificationRequest extends FormRequest
{

    public function rules()
    {
        return [
            'phone_number' => 'required|numeric|digits:11',
        ];
    }
}
