<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CreateCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Http\Resources\AdminCommenttResource;
use App\Http\Resources\CommentResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function publicComments()
    {
        $comments = Comment::with([
            'replies' => function($q) {
                $q->where('approved_at', '!=', null);
             }
            ])
            ->whereNotNull('approved_at')->whereNull('reply_id')
            ->get();

        return response()->json(CommentResource::collection($comments));
    }

    public function publicComment($id)
    {

        $comment = Comment::with(['replies' => function($q) {
                $q->where('approved_at', '!=', null);
            }
        ])->whereNotNull('approved_at')
            ->findOrFail($id);

        $comment['replyCount']= count($comment->replies);
//        $comment = Comment::with(['replies', 'comment'])
//            ->whereNotNull('approved_at')
//            ->findOrFail($id);
//
//        $finalComment = [];
//        foreach ($comment->replies as $reply){
//            if ($reply->approved_at != null){
//                array_push($finalComment, $reply->toArray());
//            };
//        }
//
//        $comment['replyCount']= count($finalComment);
//        $comment['finalReplies']= $finalComment;

        return response()->json(CommentResource::make($comment));
    }

    public function index(Request $request)
    {
        $isApproved = $request->get('approved');

        $comments = Comment::with(['post'])
        ->whereHas('post',function($q) {
            $q->where('user_id',auth()->user()->id) ;
        });
//        if ($isApproved==1) {
//            $comments->whereNotNull('approved_at');
//        }
//        if($isApproved==='0') {
//            $comments->whereNull('approved_at');
//        }

        return response()->json(CommentResource::collection($comments->get()));
    }

    public function adminIndex(Request $request)
    {
        $isApproved = $request->get('approved');
        $comments = Comment::query();
//        if ($isApproved==1) {
//            $comments->whereNotNull('approved_at');
//        }
//        if($isApproved==='0') {
//            $comments->whereNull('approved_at');
//        }

        return response()->json(CommentResource::collection($comments->get()));
    }

    public function AdminShow($id)
    {
        $comment = Comment::query()->findOrFail($id);

        return response()->json(AdminCommenttResource::make($comment));
    }

    public function show($id)
    {
        $comment = Comment::with(['post'])
            ->whereHas('post',function($q) {
                $q->where('user_id',auth()->user()->id);
            })->findOrFail($id);

        return response()->json(CommentResource::make($comment));
    }

    public function approveComment($id)
    {
        $comment = Comment::query()->whrefindOrFail($id);
        if($comment->approved_at == null){
            $comment->update([
                'approved_at' => now()
            ]);
        }
        else{
            $comment->update([
                'approved_at' => null
            ]);
        }


        return response()->json(CommentResource::make($comment));
    }

    public function store(CreateCommentRequest $request)
    {
        // php artisan make:request  CreateCommentRequest
        $comment= Comment::query()->create([
            'body'=> $request->get('body'),
            'post_id'=> $request->get('post_id'),
            'user_id'=> Auth::user()->id,
        ]);
        return response()->json(CommentResource::make($comment));
    }

    public function createCommentReply(CreateCommentRequest $request, $commentId)
    {
        // php artisan make:request  CreateCommentRequest
        $comment= Comment::query()->create([
            'body'=> $request->get('body'),
            'post_id'=> $request->get('post_id'),
            'reply_id'=> $commentId,
            'user_id'=> Auth::user()->id,
        ]);
        return response()->json(CommentResource::make($comment));
    }

    public function update(UpdateCommentRequest $request, $id)
    {
        $comment=Comment::query()->where('user_id',Auth::user()->id)->findOrFail($id);

        $comment->update([
            'body'=> $request->get('body', $comment->body),
            'approved_at'=> null
        ]);
        return response()->json(CommentResource::make($comment));
    }

    public function destroy($id)
    {
        $comment = Comment::query()->where('user_id',Auth::user()->id)->findOrFail($id);
        $comment->delete();
        return response()->json([]);

    }


}
