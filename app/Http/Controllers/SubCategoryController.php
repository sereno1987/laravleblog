<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CreateSubCategorytRequest;
use App\Http\Requests\UpdateSubCategoryRequest;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\PostResource;
use App\Http\Resources\SubCategoryResource;
use App\Post;
use App\SubCategory;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubCategoryController extends Controller
{

    public function publicSubCategory($id)
    {
        $posts = Post::with(['user'])->where('sub_category_id',$id)->where('published',1)
            ->whereHas('user', function ($query) {
                $query->where('deactivated_at', null);
            })->get();

        return response()->json(PostResource::collection($posts));
    }

    public function index()
    {
        $subCategories=SubCategory::query()->paginate();
        return response()->json(SubCategoryResource::collection($subCategories));
    }

    public function store(CreateSubCategorytRequest $request)
    {
        // php artisan make:request  CreateSubCategorytRequest
        $subCategoryExists = SubCategory::query()->where([
            'name'=> $request->get('name'),
            'category_id'=> $request->get('category_id'),
        ])->exists();

        if ($subCategoryExists) {
            return response()->json(['message' => 'subcategory already exists in this category group.']);
        }
        $subCategory= SubCategory::query()->create([
            'name'=> $request->get('name'),
            'description'=> $request->get('description'),
            'category_id'=> $request->get('category_id'),
            'user_id'=> Auth::user()->id,
        ]);

//        $subCategory = SubCategory::query()->create($request->all());
        return response()->json(SubCategoryResource::make($subCategory),201);
    }

    public function show($id)
        {
            $subCategory=SubCategory::query()->findOrFail($id);
            return response()->json(SubCategoryResource::make($subCategory));
        }

    public function update(UpdateSubCategoryRequest $request, $id)
    {
        $subCategory=SubCategory::query()->findOrFail($id);
        $subCategory->update($request->all());
        return response()->json(SubCategoryResource::make($subCategory));
    }
//
    public function destroy($id)
    {
        $subCategory=SubCategory::query()->where('user_id',Auth::user()->id)->findOrFail($id);
        $subCategory->delete();
        return response()->json('',204);

    }
}
